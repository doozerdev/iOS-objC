//
//  ItemVC.h
//  Doozer
//
//  Created by Daniel Apone on 8/3/15.
//  Copyright © 2015 Daniel Apone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "Item.h"

@interface ItemVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *solutionsTable;


@property Item * detailItem;
@property Item * parentList;
@property UIColor *themeColor;

@property float titleFieldExtraHeight;

@property BOOL showingDatePanel;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property NSMutableArray *hyperlinks;
@property NSMutableArray *solutions;
@property NSMutableArray *images;
@property NSMutableArray *cellHeights;



@end
